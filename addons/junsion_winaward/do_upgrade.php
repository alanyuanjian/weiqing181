<?php
pdo_query("CREATE TABLE IF NOT EXISTS `ims_junsion_winaward_good` (
`id` int(10) NOT NULL AUTO_INCREMENT,
`weid` int(10) DEFAULT '0',
`title` varchar(150),
`sub_title` varchar(50),
`label` varchar(10),
`logo` varchar(255),
`price` decimal(11,2) NOT NULL DEFAULT '0.00',
`postage` decimal(11,2) NOT NULL DEFAULT '0.00',
`costprice` decimal(11,2) NOT NULL DEFAULT '0.00',
`game` text,
`status` tinyint(1) DEFAULT '0' COMMENT '状态 0显示 1隐藏',
`sort` int(10) DEFAULT '0',
`buy` decimal(11,2) NOT NULL DEFAULT '0.00',
`type` tinyint(1) DEFAULT '0' COMMENT '状态 0实体商品 1虚拟商品',
PRIMARY KEY (`id`),
KEY `idx_uniacid` (`weid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ims_junsion_winaward_invite_log` (
`id` int(10) NOT NULL AUTO_INCREMENT,
`weid` int(10) DEFAULT '0',
`mid` int(10) DEFAULT '0',
`gid` int(10) DEFAULT '0',
`credit` decimal(11,2) NOT NULL DEFAULT '0.00',
`remark` varchar(50),
`createtime` int(11) DEFAULT '0',
PRIMARY KEY (`id`),
UNIQUE KEY `mc` (`mid`,`createtime`),
KEY `idx_uniacid` (`weid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ims_junsion_winaward_log` (
`id` int(10) NOT NULL AUTO_INCREMENT,
`weid` int(10) DEFAULT '0',
`oid` int(10) DEFAULT '0',
`mid` int(10) DEFAULT '0',
`gid` int(10) DEFAULT '0',
`status` tinyint(1) DEFAULT '0' COMMENT '状态 0未中奖 1已中奖',
`createtime` int(11) DEFAULT '0',
PRIMARY KEY (`id`),
UNIQUE KEY `mc` (`mid`,`createtime`),
KEY `idx_uniacid` (`weid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ims_junsion_winaward_mcredit` (
`mid` int(11) DEFAULT '0',
`credit` decimal(11,2) NOT NULL DEFAULT '0.00',
`remark` varchar(50),
`createtime` int(10) DEFAULT '0',
UNIQUE KEY `mc` (`mid`,`createtime`),
KEY `mid` (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ims_junsion_winaward_mem` (
`id` int(10) NOT NULL AUTO_INCREMENT,
`weid` int(10) DEFAULT '0',
`openid` varchar(50),
`unionid` varchar(50) COMMENT '用户unionid',
`nickname` varchar(50),
`avatar` varchar(255),
`credit` decimal(11,2) NOT NULL DEFAULT '0.00',
`status` tinyint(1) DEFAULT '0' COMMENT '状态 0正常 1禁用',
`agentid` int(11) DEFAULT '0',
`red` decimal(11,2) NOT NULL DEFAULT '0.00',
`total_red` decimal(11,2) NOT NULL DEFAULT '0.00',
`total_credit` decimal(11,2) NOT NULL DEFAULT '0.00',
`createtime` int(11) DEFAULT '0',
`isagent` tinyint(1) DEFAULT '0' COMMENT '代理 0否 1是',
`qr` varchar(255),
PRIMARY KEY (`id`),
UNIQUE KEY `uk_openid` (`openid`),
KEY `idx_uniacid` (`weid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ims_junsion_winaward_order` (
`id` int(10) NOT NULL AUTO_INCREMENT,
`weid` int(10) DEFAULT '0',
`orderno` varchar(50) COMMENT '订单编号',
`mid` int(10) DEFAULT '0',
`gid` int(10) DEFAULT '0',
`price` decimal(11,2) NOT NULL DEFAULT '0.00',
`status` tinyint(1) DEFAULT '0' COMMENT '状态 0待支付 1已支付',
`remark` varchar(150) COMMENT '备注',
`createtime` int(11) DEFAULT '0',
PRIMARY KEY (`id`),
KEY `idx_uniacid` (`weid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ims_junsion_winaward_rcredit` (
`mid` int(11) DEFAULT '0',
`aid` int(11) DEFAULT '0',
`credit` decimal(11,2) NOT NULL DEFAULT '0.00',
`remark` varchar(50),
`createtime` int(10) DEFAULT '0',
UNIQUE KEY `mc` (`mid`,`createtime`),
KEY `mid` (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ims_junsion_winaward_recharge` (
`id` int(10) NOT NULL AUTO_INCREMENT,
`weid` int(10) DEFAULT '0',
`orderno` varchar(50) COMMENT '订单编号',
`mid` int(10) DEFAULT '0',
`price` decimal(11,2) NOT NULL DEFAULT '0.00',
`commission` decimal(11,2) NOT NULL DEFAULT '0.00',
`commission_red` decimal(11,2) NOT NULL DEFAULT '0.00',
`status` tinyint(1) DEFAULT '0' COMMENT '状态 0待支付 1已支付',
`transid` varchar(50) COMMENT '流水单号',
`createtime` int(11) DEFAULT '0',
`commission2` decimal(11,2) NOT NULL DEFAULT '0.00',
`commission3` decimal(11,2) NOT NULL DEFAULT '0.00',
`commission_red2` decimal(11,2) NOT NULL DEFAULT '0.00',
`commission_red3` decimal(11,2) NOT NULL DEFAULT '0.00',
PRIMARY KEY (`id`),
KEY `idx_uniacid` (`weid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ims_junsion_winaward_rouge` (
`id` int(10) NOT NULL AUTO_INCREMENT,
`weid` int(10) DEFAULT '0',
`orderno` varchar(50) COMMENT '订单编号',
`oid` int(10) DEFAULT '0',
`mid` int(10) DEFAULT '0',
`gid` int(10) DEFAULT '0',
`price` decimal(11,2) NOT NULL DEFAULT '0.00',
`status` tinyint(1) DEFAULT '0' COMMENT '状态 0待支付 1已支付 2已发货',
`transid` varchar(50) COMMENT '流水单号',
`uname` varchar(50),
`mobile` varchar(12),
`addr` varchar(12),
`express` int(11) DEFAULT '0' COMMENT '快递公司编号',
`expressno` varchar(50) COMMENT '快递单号',
`createtime` int(11) DEFAULT '0',
`unit_price` decimal(11,2) NOT NULL DEFAULT '0.00',
`num` int(10) DEFAULT '1',
`wx_no` varchar(50) COMMENT '虚拟商品必填',
PRIMARY KEY (`id`),
KEY `idx_uniacid` (`weid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ims_junsion_winaward_session` (
`openid` varchar(50),
`session_key` varchar(50),
UNIQUE KEY `m_hid` (`openid`,`session_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ims_junsion_winaward_with` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`weid` int(11) DEFAULT '0',
`mid` int(11) DEFAULT '0',
`price` decimal(11,2) NOT NULL DEFAULT '0.00',
`wrate` decimal(11,2) NOT NULL DEFAULT '0.00',
`status` tinyint(1) DEFAULT '0',
`transid` varchar(50),
`createtime` int(10) DEFAULT '0',
PRIMARY KEY (`id`),
UNIQUE KEY `uk_mtime` (`mid`,`createtime`),
KEY `weid` (`weid`),
KEY `mid` (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

");
if(pdo_tableexists('junsion_winaward_good')) {
	if(!pdo_fieldexists('junsion_winaward_good',  'id')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_good')." ADD `id` int(10) NOT NULL AUTO_INCREMENT;");
	}	
}
if(pdo_tableexists('junsion_winaward_good')) {
	if(!pdo_fieldexists('junsion_winaward_good',  'weid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_good')." ADD `weid` int(10) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_good')) {
	if(!pdo_fieldexists('junsion_winaward_good',  'title')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_good')." ADD `title` varchar(150);");
	}	
}
if(pdo_tableexists('junsion_winaward_good')) {
	if(!pdo_fieldexists('junsion_winaward_good',  'sub_title')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_good')." ADD `sub_title` varchar(50);");
	}	
}
if(pdo_tableexists('junsion_winaward_good')) {
	if(!pdo_fieldexists('junsion_winaward_good',  'label')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_good')." ADD `label` varchar(10);");
	}	
}
if(pdo_tableexists('junsion_winaward_good')) {
	if(!pdo_fieldexists('junsion_winaward_good',  'logo')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_good')." ADD `logo` varchar(255);");
	}	
}
if(pdo_tableexists('junsion_winaward_good')) {
	if(!pdo_fieldexists('junsion_winaward_good',  'price')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_good')." ADD `price` decimal(11,2) NOT NULL DEFAULT '0.00';");
	}	
}
if(pdo_tableexists('junsion_winaward_good')) {
	if(!pdo_fieldexists('junsion_winaward_good',  'postage')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_good')." ADD `postage` decimal(11,2) NOT NULL DEFAULT '0.00';");
	}	
}
if(pdo_tableexists('junsion_winaward_good')) {
	if(!pdo_fieldexists('junsion_winaward_good',  'costprice')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_good')." ADD `costprice` decimal(11,2) NOT NULL DEFAULT '0.00';");
	}	
}
if(pdo_tableexists('junsion_winaward_good')) {
	if(!pdo_fieldexists('junsion_winaward_good',  'game')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_good')." ADD `game` text;");
	}	
}
if(pdo_tableexists('junsion_winaward_good')) {
	if(!pdo_fieldexists('junsion_winaward_good',  'status')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_good')." ADD `status` tinyint(1) DEFAULT '0' COMMENT '状态 0显示 1隐藏';");
	}	
}
if(pdo_tableexists('junsion_winaward_good')) {
	if(!pdo_fieldexists('junsion_winaward_good',  'sort')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_good')." ADD `sort` int(10) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_good')) {
	if(!pdo_fieldexists('junsion_winaward_good',  'buy')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_good')." ADD `buy` decimal(11,2) NOT NULL DEFAULT '0.00';");
	}	
}
if(pdo_tableexists('junsion_winaward_good')) {
	if(!pdo_fieldexists('junsion_winaward_good',  'type')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_good')." ADD `type` tinyint(1) DEFAULT '0' COMMENT '状态 0实体商品 1虚拟商品';");
	}	
}
if(pdo_tableexists('junsion_winaward_invite_log')) {
	if(!pdo_fieldexists('junsion_winaward_invite_log',  'id')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_invite_log')." ADD `id` int(10) NOT NULL AUTO_INCREMENT;");
	}	
}
if(pdo_tableexists('junsion_winaward_invite_log')) {
	if(!pdo_fieldexists('junsion_winaward_invite_log',  'weid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_invite_log')." ADD `weid` int(10) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_invite_log')) {
	if(!pdo_fieldexists('junsion_winaward_invite_log',  'mid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_invite_log')." ADD `mid` int(10) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_invite_log')) {
	if(!pdo_fieldexists('junsion_winaward_invite_log',  'gid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_invite_log')." ADD `gid` int(10) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_invite_log')) {
	if(!pdo_fieldexists('junsion_winaward_invite_log',  'credit')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_invite_log')." ADD `credit` decimal(11,2) NOT NULL DEFAULT '0.00';");
	}	
}
if(pdo_tableexists('junsion_winaward_invite_log')) {
	if(!pdo_fieldexists('junsion_winaward_invite_log',  'remark')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_invite_log')." ADD `remark` varchar(50);");
	}	
}
if(pdo_tableexists('junsion_winaward_invite_log')) {
	if(!pdo_fieldexists('junsion_winaward_invite_log',  'createtime')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_invite_log')." ADD `createtime` int(11) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_log')) {
	if(!pdo_fieldexists('junsion_winaward_log',  'id')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_log')." ADD `id` int(10) NOT NULL AUTO_INCREMENT;");
	}	
}
if(pdo_tableexists('junsion_winaward_log')) {
	if(!pdo_fieldexists('junsion_winaward_log',  'weid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_log')." ADD `weid` int(10) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_log')) {
	if(!pdo_fieldexists('junsion_winaward_log',  'oid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_log')." ADD `oid` int(10) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_log')) {
	if(!pdo_fieldexists('junsion_winaward_log',  'mid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_log')." ADD `mid` int(10) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_log')) {
	if(!pdo_fieldexists('junsion_winaward_log',  'gid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_log')." ADD `gid` int(10) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_log')) {
	if(!pdo_fieldexists('junsion_winaward_log',  'status')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_log')." ADD `status` tinyint(1) DEFAULT '0' COMMENT '状态 0未中奖 1已中奖';");
	}	
}
if(pdo_tableexists('junsion_winaward_log')) {
	if(!pdo_fieldexists('junsion_winaward_log',  'createtime')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_log')." ADD `createtime` int(11) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_mcredit')) {
	if(!pdo_fieldexists('junsion_winaward_mcredit',  'mid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_mcredit')." ADD `mid` int(11) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_mcredit')) {
	if(!pdo_fieldexists('junsion_winaward_mcredit',  'credit')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_mcredit')." ADD `credit` decimal(11,2) NOT NULL DEFAULT '0.00';");
	}	
}
if(pdo_tableexists('junsion_winaward_mcredit')) {
	if(!pdo_fieldexists('junsion_winaward_mcredit',  'remark')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_mcredit')." ADD `remark` varchar(50);");
	}	
}
if(pdo_tableexists('junsion_winaward_mcredit')) {
	if(!pdo_fieldexists('junsion_winaward_mcredit',  'createtime')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_mcredit')." ADD `createtime` int(10) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_mem')) {
	if(!pdo_fieldexists('junsion_winaward_mem',  'id')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_mem')." ADD `id` int(10) NOT NULL AUTO_INCREMENT;");
	}	
}
if(pdo_tableexists('junsion_winaward_mem')) {
	if(!pdo_fieldexists('junsion_winaward_mem',  'weid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_mem')." ADD `weid` int(10) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_mem')) {
	if(!pdo_fieldexists('junsion_winaward_mem',  'openid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_mem')." ADD `openid` varchar(50);");
	}	
}
if(pdo_tableexists('junsion_winaward_mem')) {
	if(!pdo_fieldexists('junsion_winaward_mem',  'unionid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_mem')." ADD `unionid` varchar(50) COMMENT '用户unionid';");
	}	
}
if(pdo_tableexists('junsion_winaward_mem')) {
	if(!pdo_fieldexists('junsion_winaward_mem',  'nickname')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_mem')." ADD `nickname` varchar(50);");
	}	
}
if(pdo_tableexists('junsion_winaward_mem')) {
	if(!pdo_fieldexists('junsion_winaward_mem',  'avatar')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_mem')." ADD `avatar` varchar(255);");
	}	
}
if(pdo_tableexists('junsion_winaward_mem')) {
	if(!pdo_fieldexists('junsion_winaward_mem',  'credit')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_mem')." ADD `credit` decimal(11,2) NOT NULL DEFAULT '0.00';");
	}	
}
if(pdo_tableexists('junsion_winaward_mem')) {
	if(!pdo_fieldexists('junsion_winaward_mem',  'status')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_mem')." ADD `status` tinyint(1) DEFAULT '0' COMMENT '状态 0正常 1禁用';");
	}	
}
if(pdo_tableexists('junsion_winaward_mem')) {
	if(!pdo_fieldexists('junsion_winaward_mem',  'agentid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_mem')." ADD `agentid` int(11) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_mem')) {
	if(!pdo_fieldexists('junsion_winaward_mem',  'red')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_mem')." ADD `red` decimal(11,2) NOT NULL DEFAULT '0.00';");
	}	
}
if(pdo_tableexists('junsion_winaward_mem')) {
	if(!pdo_fieldexists('junsion_winaward_mem',  'total_red')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_mem')." ADD `total_red` decimal(11,2) NOT NULL DEFAULT '0.00';");
	}	
}
if(pdo_tableexists('junsion_winaward_mem')) {
	if(!pdo_fieldexists('junsion_winaward_mem',  'total_credit')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_mem')." ADD `total_credit` decimal(11,2) NOT NULL DEFAULT '0.00';");
	}	
}
if(pdo_tableexists('junsion_winaward_mem')) {
	if(!pdo_fieldexists('junsion_winaward_mem',  'createtime')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_mem')." ADD `createtime` int(11) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_mem')) {
	if(!pdo_fieldexists('junsion_winaward_mem',  'isagent')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_mem')." ADD `isagent` tinyint(1) DEFAULT '0' COMMENT '代理 0否 1是';");
	}	
}
if(pdo_tableexists('junsion_winaward_mem')) {
	if(!pdo_fieldexists('junsion_winaward_mem',  'qr')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_mem')." ADD `qr` varchar(255);");
	}	
}
if(pdo_tableexists('junsion_winaward_order')) {
	if(!pdo_fieldexists('junsion_winaward_order',  'id')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_order')." ADD `id` int(10) NOT NULL AUTO_INCREMENT;");
	}	
}
if(pdo_tableexists('junsion_winaward_order')) {
	if(!pdo_fieldexists('junsion_winaward_order',  'weid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_order')." ADD `weid` int(10) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_order')) {
	if(!pdo_fieldexists('junsion_winaward_order',  'orderno')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_order')." ADD `orderno` varchar(50) COMMENT '订单编号';");
	}	
}
if(pdo_tableexists('junsion_winaward_order')) {
	if(!pdo_fieldexists('junsion_winaward_order',  'mid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_order')." ADD `mid` int(10) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_order')) {
	if(!pdo_fieldexists('junsion_winaward_order',  'gid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_order')." ADD `gid` int(10) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_order')) {
	if(!pdo_fieldexists('junsion_winaward_order',  'price')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_order')." ADD `price` decimal(11,2) NOT NULL DEFAULT '0.00';");
	}	
}
if(pdo_tableexists('junsion_winaward_order')) {
	if(!pdo_fieldexists('junsion_winaward_order',  'status')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_order')." ADD `status` tinyint(1) DEFAULT '0' COMMENT '状态 0待支付 1已支付';");
	}	
}
if(pdo_tableexists('junsion_winaward_order')) {
	if(!pdo_fieldexists('junsion_winaward_order',  'remark')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_order')." ADD `remark` varchar(150) COMMENT '备注';");
	}	
}
if(pdo_tableexists('junsion_winaward_order')) {
	if(!pdo_fieldexists('junsion_winaward_order',  'createtime')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_order')." ADD `createtime` int(11) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_rcredit')) {
	if(!pdo_fieldexists('junsion_winaward_rcredit',  'mid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_rcredit')." ADD `mid` int(11) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_rcredit')) {
	if(!pdo_fieldexists('junsion_winaward_rcredit',  'aid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_rcredit')." ADD `aid` int(11) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_rcredit')) {
	if(!pdo_fieldexists('junsion_winaward_rcredit',  'credit')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_rcredit')." ADD `credit` decimal(11,2) NOT NULL DEFAULT '0.00';");
	}	
}
if(pdo_tableexists('junsion_winaward_rcredit')) {
	if(!pdo_fieldexists('junsion_winaward_rcredit',  'remark')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_rcredit')." ADD `remark` varchar(50);");
	}	
}
if(pdo_tableexists('junsion_winaward_rcredit')) {
	if(!pdo_fieldexists('junsion_winaward_rcredit',  'createtime')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_rcredit')." ADD `createtime` int(10) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_recharge')) {
	if(!pdo_fieldexists('junsion_winaward_recharge',  'id')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_recharge')." ADD `id` int(10) NOT NULL AUTO_INCREMENT;");
	}	
}
if(pdo_tableexists('junsion_winaward_recharge')) {
	if(!pdo_fieldexists('junsion_winaward_recharge',  'weid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_recharge')." ADD `weid` int(10) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_recharge')) {
	if(!pdo_fieldexists('junsion_winaward_recharge',  'orderno')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_recharge')." ADD `orderno` varchar(50) COMMENT '订单编号';");
	}	
}
if(pdo_tableexists('junsion_winaward_recharge')) {
	if(!pdo_fieldexists('junsion_winaward_recharge',  'mid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_recharge')." ADD `mid` int(10) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_recharge')) {
	if(!pdo_fieldexists('junsion_winaward_recharge',  'price')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_recharge')." ADD `price` decimal(11,2) NOT NULL DEFAULT '0.00';");
	}	
}
if(pdo_tableexists('junsion_winaward_recharge')) {
	if(!pdo_fieldexists('junsion_winaward_recharge',  'commission')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_recharge')." ADD `commission` decimal(11,2) NOT NULL DEFAULT '0.00';");
	}	
}
if(pdo_tableexists('junsion_winaward_recharge')) {
	if(!pdo_fieldexists('junsion_winaward_recharge',  'commission_red')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_recharge')." ADD `commission_red` decimal(11,2) NOT NULL DEFAULT '0.00';");
	}	
}
if(pdo_tableexists('junsion_winaward_recharge')) {
	if(!pdo_fieldexists('junsion_winaward_recharge',  'status')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_recharge')." ADD `status` tinyint(1) DEFAULT '0' COMMENT '状态 0待支付 1已支付';");
	}	
}
if(pdo_tableexists('junsion_winaward_recharge')) {
	if(!pdo_fieldexists('junsion_winaward_recharge',  'transid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_recharge')." ADD `transid` varchar(50) COMMENT '流水单号';");
	}	
}
if(pdo_tableexists('junsion_winaward_recharge')) {
	if(!pdo_fieldexists('junsion_winaward_recharge',  'createtime')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_recharge')." ADD `createtime` int(11) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_recharge')) {
	if(!pdo_fieldexists('junsion_winaward_recharge',  'commission2')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_recharge')." ADD `commission2` decimal(11,2) NOT NULL DEFAULT '0.00';");
	}	
}
if(pdo_tableexists('junsion_winaward_recharge')) {
	if(!pdo_fieldexists('junsion_winaward_recharge',  'commission3')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_recharge')." ADD `commission3` decimal(11,2) NOT NULL DEFAULT '0.00';");
	}	
}
if(pdo_tableexists('junsion_winaward_recharge')) {
	if(!pdo_fieldexists('junsion_winaward_recharge',  'commission_red2')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_recharge')." ADD `commission_red2` decimal(11,2) NOT NULL DEFAULT '0.00';");
	}	
}
if(pdo_tableexists('junsion_winaward_recharge')) {
	if(!pdo_fieldexists('junsion_winaward_recharge',  'commission_red3')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_recharge')." ADD `commission_red3` decimal(11,2) NOT NULL DEFAULT '0.00';");
	}	
}
if(pdo_tableexists('junsion_winaward_rouge')) {
	if(!pdo_fieldexists('junsion_winaward_rouge',  'id')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_rouge')." ADD `id` int(10) NOT NULL AUTO_INCREMENT;");
	}	
}
if(pdo_tableexists('junsion_winaward_rouge')) {
	if(!pdo_fieldexists('junsion_winaward_rouge',  'weid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_rouge')." ADD `weid` int(10) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_rouge')) {
	if(!pdo_fieldexists('junsion_winaward_rouge',  'orderno')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_rouge')." ADD `orderno` varchar(50) COMMENT '订单编号';");
	}	
}
if(pdo_tableexists('junsion_winaward_rouge')) {
	if(!pdo_fieldexists('junsion_winaward_rouge',  'oid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_rouge')." ADD `oid` int(10) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_rouge')) {
	if(!pdo_fieldexists('junsion_winaward_rouge',  'mid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_rouge')." ADD `mid` int(10) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_rouge')) {
	if(!pdo_fieldexists('junsion_winaward_rouge',  'gid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_rouge')." ADD `gid` int(10) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_rouge')) {
	if(!pdo_fieldexists('junsion_winaward_rouge',  'price')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_rouge')." ADD `price` decimal(11,2) NOT NULL DEFAULT '0.00';");
	}	
}
if(pdo_tableexists('junsion_winaward_rouge')) {
	if(!pdo_fieldexists('junsion_winaward_rouge',  'status')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_rouge')." ADD `status` tinyint(1) DEFAULT '0' COMMENT '状态 0待支付 1已支付 2已发货';");
	}	
}
if(pdo_tableexists('junsion_winaward_rouge')) {
	if(!pdo_fieldexists('junsion_winaward_rouge',  'transid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_rouge')." ADD `transid` varchar(50) COMMENT '流水单号';");
	}	
}
if(pdo_tableexists('junsion_winaward_rouge')) {
	if(!pdo_fieldexists('junsion_winaward_rouge',  'uname')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_rouge')." ADD `uname` varchar(50);");
	}	
}
if(pdo_tableexists('junsion_winaward_rouge')) {
	if(!pdo_fieldexists('junsion_winaward_rouge',  'mobile')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_rouge')." ADD `mobile` varchar(12);");
	}	
}
if(pdo_tableexists('junsion_winaward_rouge')) {
	if(!pdo_fieldexists('junsion_winaward_rouge',  'addr')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_rouge')." ADD `addr` varchar(12);");
	}	
}
if(pdo_tableexists('junsion_winaward_rouge')) {
	if(!pdo_fieldexists('junsion_winaward_rouge',  'express')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_rouge')." ADD `express` int(11) DEFAULT '0' COMMENT '快递公司编号';");
	}	
}
if(pdo_tableexists('junsion_winaward_rouge')) {
	if(!pdo_fieldexists('junsion_winaward_rouge',  'expressno')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_rouge')." ADD `expressno` varchar(50) COMMENT '快递单号';");
	}	
}
if(pdo_tableexists('junsion_winaward_rouge')) {
	if(!pdo_fieldexists('junsion_winaward_rouge',  'createtime')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_rouge')." ADD `createtime` int(11) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_rouge')) {
	if(!pdo_fieldexists('junsion_winaward_rouge',  'unit_price')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_rouge')." ADD `unit_price` decimal(11,2) NOT NULL DEFAULT '0.00';");
	}	
}
if(pdo_tableexists('junsion_winaward_rouge')) {
	if(!pdo_fieldexists('junsion_winaward_rouge',  'num')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_rouge')." ADD `num` int(10) DEFAULT '1';");
	}	
}
if(pdo_tableexists('junsion_winaward_rouge')) {
	if(!pdo_fieldexists('junsion_winaward_rouge',  'wx_no')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_rouge')." ADD `wx_no` varchar(50) COMMENT '虚拟商品必填';");
	}	
}
if(pdo_tableexists('junsion_winaward_session')) {
	if(!pdo_fieldexists('junsion_winaward_session',  'openid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_session')." ADD `openid` varchar(50);");
	}	
}
if(pdo_tableexists('junsion_winaward_session')) {
	if(!pdo_fieldexists('junsion_winaward_session',  'session_key')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_session')." ADD `session_key` varchar(50);");
	}	
}
if(pdo_tableexists('junsion_winaward_with')) {
	if(!pdo_fieldexists('junsion_winaward_with',  'id')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_with')." ADD `id` int(11) NOT NULL AUTO_INCREMENT;");
	}	
}
if(pdo_tableexists('junsion_winaward_with')) {
	if(!pdo_fieldexists('junsion_winaward_with',  'weid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_with')." ADD `weid` int(11) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_with')) {
	if(!pdo_fieldexists('junsion_winaward_with',  'mid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_with')." ADD `mid` int(11) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_with')) {
	if(!pdo_fieldexists('junsion_winaward_with',  'price')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_with')." ADD `price` decimal(11,2) NOT NULL DEFAULT '0.00';");
	}	
}
if(pdo_tableexists('junsion_winaward_with')) {
	if(!pdo_fieldexists('junsion_winaward_with',  'wrate')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_with')." ADD `wrate` decimal(11,2) NOT NULL DEFAULT '0.00';");
	}	
}
if(pdo_tableexists('junsion_winaward_with')) {
	if(!pdo_fieldexists('junsion_winaward_with',  'status')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_with')." ADD `status` tinyint(1) DEFAULT '0';");
	}	
}
if(pdo_tableexists('junsion_winaward_with')) {
	if(!pdo_fieldexists('junsion_winaward_with',  'transid')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_with')." ADD `transid` varchar(50);");
	}	
}
if(pdo_tableexists('junsion_winaward_with')) {
	if(!pdo_fieldexists('junsion_winaward_with',  'createtime')) {
		pdo_query("ALTER TABLE ".tablename('junsion_winaward_with')." ADD `createtime` int(10) DEFAULT '0';");
	}	
}
